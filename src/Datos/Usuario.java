package Datos;
import java.io.*;

import Logica.Puntajes;
public class Usuario {

	// recibe un usuario y clave y lo guarda en un archivo; el archivo tiene el
	// nombre del usuario y adentro tiene la clave
		public boolean crearUsuario(String nombre, String clave) {
			try {
				File archivo = new File(nombre + ".txt");
				FileWriter escribir = new FileWriter(archivo, true);
				String nuevalinea = System.getProperty("line.separator");
				escribir.write("password-"+clave+nuevalinea);
				escribir.write("1.matematicas- prom 0 min 0 max 0 cont 0"+nuevalinea);
				escribir.write("2.scramble- prom 0 min 0 max 0 cont 0"+nuevalinea);
				escribir.write("3.memorice- prom 0 min 0 max 0 cont 0"+nuevalinea);
				escribir.write("4.gato- prom 0 min 0 max 0 cont 0");
				escribir.close();
				return true;
			}
			catch (Exception e) {
				System.out.println("Error al escribir");
				return false;
			}
		}
		// retorna false si la clave y el usuario no coinciden
		public boolean leerArchivo(String nombre, String clave) {
			boolean validador = false;
			String claveObtenida = "";
			try {
				FileReader lector = new FileReader(nombre + ".txt");
				BufferedReader contenido = new BufferedReader(lector);
				if ((claveObtenida = contenido.readLine()) != null) {
					claveObtenida=claveObtenida.split("-")[1];
					System.out.println("se recupero la clasve y es: "+claveObtenida);
					return claveObtenida.equals(clave);
				}
			} catch (Exception e) {
				return false;
			}
			return validador;
		}
			
		public boolean existeUsuario(String nombre) {
			try {
				FileReader lector = new FileReader(nombre + ".txt");
				BufferedReader contenido = new BufferedReader(lector);
				String a = contenido.readLine();
				contenido.close();
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		public void registrarPuntaje(String puntaje)
		{
			try {
				FileReader lector = new FileReader(puntaje.split(" ")[0]+".txt");
				BufferedReader contenido = new BufferedReader(lector);
				String a ="";
				String letra="";
				String nuevalinea = System.getProperty("line.separator");
				while((a=contenido.readLine())!=null)
				{
					System.out.println(a);
					
					if(puntaje.contains("matematicas")&&a.contains("matematicas"))
					{
						letra+="1.matematicas prom "+puntaje.split(" ")[4]+" min "+puntaje.split(" ")[6]+" max "+puntaje.split(" ")[8]
								+" cont "+puntaje.split(" ")[10]+nuevalinea;
					}
					else if(puntaje.contains("scramble")&&a.contains("scramble"))
					{
						letra+="2.scramble prom "+puntaje.split(" ")[4]+" min "+puntaje.split(" ")[6]+" max "+puntaje.split(" ")[8]
								+" cont "+puntaje.split(" ")[10]+nuevalinea;
					}
					else if(puntaje.contains("memorice")&&a.contains("memorice"))
					{
						letra+="3.memorices prom "+puntaje.split(" ")[4]+" min "+puntaje.split(" ")[6]+" max "+puntaje.split(" ")[8]
								+" cont "+puntaje.split(" ")[10]+nuevalinea;
					}
					else if(puntaje.contains("gato")&&a.contains("gato"))
					{
						letra+="4.gato prom "+puntaje.split(" ")[4]+" min "+puntaje.split(" ")[6]+" max "+puntaje.split(" ")[8]+
								" cont "+puntaje.split(" ")[10] + nuevalinea;
					}
					else
						letra+=a+nuevalinea;
				}
				contenido.close();
				FileWriter w = new FileWriter(puntaje.split(" ")[0]+".txt");
				BufferedWriter bw = new BufferedWriter(w);
				PrintWriter wr = new PrintWriter(bw);
				wr.write(letra);
				wr.close();bw.close();
			} catch (Exception e) {
			}
		}
		public Puntajes puntajesUsuario(String user)
		{
			Puntajes p;
			FileReader lector;
			try {
				lector = new FileReader(user+".txt");
				BufferedReader contenido = new BufferedReader(lector);
				String a ="";
				int[] mat=new int[4];
				int[] scra= new int[4];
				int[] mem= new int[4];
				int[] gato= new int[4];
				String nuevalinea = System.getProperty("line.separator");
				while((a=contenido.readLine())!=null)
				{
					if(a.contains("matematica"))
					{
						mat[0]=Integer.parseInt(a.split(" ")[2]);
						mat[1]=Integer.parseInt(a.split(" ")[4]);
						mat[2]=Integer.parseInt(a.split(" ")[6]);
						mat[3]=Integer.parseInt(a.split(" ")[8]);
					}
					else if(a.contains("memorice"))
					{
						mem[0]=Integer.parseInt(a.split(" ")[2]);
						mem[1]=Integer.parseInt(a.split(" ")[4]);
						mem[2]=Integer.parseInt(a.split(" ")[6]);
						mem[3]=Integer.parseInt(a.split(" ")[8]);
					}else if(a.contains("scramble"))
					{
						scra[0]=Integer.parseInt(a.split(" ")[2]);
						scra[1]=Integer.parseInt(a.split(" ")[4]);
						scra[2]=Integer.parseInt(a.split(" ")[6]);
						scra[3]=Integer.parseInt(a.split(" ")[8]);
					}else if(a.contains("gato"))
					{
						gato[0]=Integer.parseInt(a.split(" ")[2]);
						gato[1]=Integer.parseInt(a.split(" ")[4]);
						gato[2]=Integer.parseInt(a.split(" ")[6]);
						gato[3]=Integer.parseInt(a.split(" ")[8]);
					}
				}
				p=new Puntajes(mat, scra, mem, gato);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				p= null;
			}
			return p;
		}
}
