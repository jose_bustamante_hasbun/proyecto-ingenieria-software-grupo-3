package Test;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Logica.JuegoMatematicas;

public class JuegoMatematicasTest {

	JuegoMatematicas tester = new JuegoMatematicas();

	@Before
	public void jugada() {

		tester.jugar();

	}

	@Test
	public void testImprimir() {
		assertNotNull("Error al generar oiperaciones del juego",tester.imprimir());
	}

	@Test
	public void testGenerar() {
		tester.generar();
		int a = tester.resultado1;
		int b = tester.resultado2;
		assertNotEquals("Se generaron resultados identicos",a,b);
	}

	@Test
	public void testDevolverSigno() {
		tester.imprimir();
		boolean a = tester.s!="";
		assertTrue("no se genera correctamente el signo de la operacion",a);
	}

}
