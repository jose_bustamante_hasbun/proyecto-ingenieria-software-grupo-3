package Test;

import static org.junit.Assert.*;

import org.junit.*;

import Logica.Puntajes;

public class PuntajesTest {

	private boolean juegos(int numero) {
		Puntajes tester = new Puntajes(null, null, null, null);
		int[] minimo = tester.puntajeMinimo;
		int[] promedio = tester.puntajePromedio;
		int[] maximo = tester.puntajeMaximo;
		int a, b, c;
		a = minimo[numero];
		b = promedio[numero];
		c = maximo[numero];
		System.out.print("" + a + b + c);
		
		if(numero==0)
			tester.setPuntajeMatematica(33);
		else if(numero==1)
			tester.setPuntajeMemorice(33);
		else
			tester.setPuntajeScramble(33);
		minimo = tester.puntajeMinimo;
		promedio = tester.puntajePromedio;
		maximo = tester.puntajeMaximo;
		a = minimo[numero];
		b = promedio[numero];
		c = maximo[numero];
		System.out.print("" + a + b + c);
		return a == minimo[0] && b == promedio[0] && c == maximo[0];
	}

	@Test
	public void testSetPuntajeMatematica() {

		assertTrue("No se modificaron los puntajes", juegos(0));

	}

	@Test
	public void testSetPuntajeScramble() {
		assertTrue("No se modificaron los puntajes", juegos(1));
	}

	@Test
	public void testSetPuntajeMemorice() {
		assertTrue("No se modificaron los puntajes", juegos(2));
	}

}
