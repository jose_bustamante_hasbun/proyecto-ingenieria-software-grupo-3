package Test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.*;

import Datos.Usuario;

public class UsuarioTest {

	Usuario tester = new Usuario();

	private void borrarPrueba(String nombre) {
		File borrar = new File(nombre + ".txt");
		assertTrue("prueba no borrada", borrar.delete());
		borrar.deleteOnExit();
	}

	@Test
	public void testCrearUsuario() {
		String usuario = "prueba";
		String clave = "prueba";
		tester.crearUsuario(usuario, clave);
		try {
			FileReader lector = new FileReader(usuario + ".txt");
			BufferedReader contenido = new BufferedReader(lector);
			String a = contenido.readLine();
			contenido.close();
			System.out.print(a);
			System.out.print(clave);
			assertTrue(
					"no se genera correctamente el archivo de usuario y clave",
					a.equals(clave));
			borrarPrueba(usuario);

		} catch (Exception e) {

		}
	}

	@Test
	public void testLeerArchivo() {
		String nombre = "prueba";
		String clave = "prueba";
		tester.crearUsuario(nombre, clave);
		assertTrue("No se encontr� el nombre de usuario de prueba",
				tester.leerArchivo(nombre, clave));
		borrarPrueba(nombre);
	}

	@Test
	public void testExisteUsuario() {
		String nombre = "prueba";
		String clave = "prueba";
		tester.crearUsuario(nombre, clave);
		assertTrue("No se encontr� el nombre de usuario de prueba",
				tester.existeUsuario(nombre));
		borrarPrueba(nombre);
	}

}
