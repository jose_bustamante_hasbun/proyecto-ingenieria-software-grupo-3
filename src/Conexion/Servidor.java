package Conexion;
import java.net.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Datos.Usuario;

public class Servidor implements Runnable
{  private ServerSocket     server = null;
   private Thread           thread = null;
   //private ChatServerThread client = null;
   private ChatServerThread clients[] = new ChatServerThread[10];
   private String info;
   
   
   //Esto es parte grafica
   private JFrame ventana = null;
   private JTextArea  display = new JTextArea();
   private JTextField input   = new JTextField();
   private JButton    send    = new JButton("Send"), connect = new JButton("Connect"),
                     quit    = new JButton("Bye");
   private String    serverName = "localhost";
   
   private int i = 0;
   
   private Usuario user;

   public Servidor()
   
   {  //Esto es nuevo 
	   user= new Usuario();
	   int port = 5555;
		ventana = new JFrame("Esperando Conexion");
		ventana.setLayout(new FlowLayout());
		ventana.setVisible(true);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	      Panel keys = new Panel(); keys.setLayout(new GridLayout(1,2));
	      
	      Panel south = new Panel(); south.setLayout(new BorderLayout());
	      
	      Label title = new Label("Servidor", Label.CENTER);
	      title.setFont(new Font("Helvetica", Font.BOLD, 14));
	      
	      ventana.setLayout(new BorderLayout());
	      ventana.add("North", title); 
	      ventana.add("Center", display);  
	      
	      ventana.setSize(200,500);
	   
	      try {
				System.out.println("Binding to port " + port + ", please wait  ...");
				server = new ServerSocket(port);
				System.out.println("Server started: " + server);
				start();
				ServidorTicTac st = new ServidorTicTac();
				st.run();
			} catch (IOException ioe) {
				System.out.println(ioe);
			} catch (Exception e) {
				System.out.println(e);
			}
   }
   public void run()
   {  while (thread != null)
      {  try
         {  
    	  System.out.println("Waiting for a client ..."); 
    	  //se espera un cliente. Cuando llega, se ejecuta la linea siguiente
            addThread(server.accept());
         }
         catch(IOException ie)
         {  System.out.println("Acceptance Error: " + ie); }
      }
   }
   public void addThread(Socket socket)
   
   {  //Cuando llega un clinte, avisa que acepta y lo crea en su Thread
	   
	   System.out.println("Client accepted: " + socket);
      clients[i] = new ChatServerThread(this, socket);
      try
      {  clients[i].open();
         clients[i].start();
         i++;
      }
      catch(IOException ioe)
      {  System.out.println("Error opening thread: " + ioe); }
   }
   public void start()
   {  if (thread == null)
      {  thread = new Thread(this); 
         thread.start();
      }
   }
   public void stop()
   {  if (thread != null)
      {  thread.stop(); 
         thread = null;
      }
   }
   public String getInfo(){
	   
	   return info;
   }
   
   
   
   public class ChatServerThread extends Thread
   {  private Socket          socket   = null;
      private Servidor      server   = null;
      private int             ID       = -1;
      private DataInputStream streamIn =  null;

      public ChatServerThread(Servidor _server, Socket _socket)
      {  server = _server;  socket = _socket;  ID = socket.getPort();
      }
      public void run()
      {  System.out.println("Server Thread " + ID + " running.");
         while (true)
         {  try
            {  
        	 //Aqui se escribe lo que el cliente manda en la consola
        	 
        	 info = streamIn.readUTF();
        	 
        	 display.append( info + "\n");
        	 if( info.contains("puntaje"))
        	 {
	        	user.registrarPuntaje(info);
        	 }
            }
            catch(IOException ioe) {  }
         }
      }
      public void open() throws IOException
      {  streamIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
      }
      public void close() throws IOException
      {  if (socket != null)    socket.close();
         if (streamIn != null)  streamIn.close();
      }
   }
}