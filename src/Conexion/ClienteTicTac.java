package Conexion;

import interfaz.SelectorJuego;
import interfaz.VentanaTicTac.MyRunnable;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * SIMBOLOGIA: Client -> Server Server -> Client ----------------
 * ---------------- MOVE <n> (0 <= n <= 8) WELCOME <char> (char in {X, O}) QUIT
 * VALID_MOVE OTHER_PLAYER_MOVED <n> VICTORY DEFEAT TIE MESSAGE <text>
 * 
 */
public class ClienteTicTac {

	public JFrame frame = new JFrame("Tic Tac Toe");
	private JLabel messageLabel = new JLabel("");
	private ImageIcon icon;
	private ImageIcon opponentIcon;

	private Square[] board = new Square[9];
	private Square currentSquare;

	private static int PORT = 8901;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;

	public Cliente cliente;
	public int win;// 0 perdio, 1 gano, 2 empate
	public int pje;

	public ClienteTicTac(String serverAddress, Cliente c) throws Exception {

		cliente = c;

		// networking
		socket = new Socket(serverAddress, PORT);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		// UI
		messageLabel.setBackground(Color.lightGray);
		frame.getContentPane().add(messageLabel, "South");

		JPanel boardPanel = new JPanel();
		boardPanel.setBackground(Color.black);
		boardPanel.setLayout(new GridLayout(3, 3, 2, 2));
		for (int i = 0; i < board.length; i++) {
			final int j = i;
			board[i] = new Square();
			board[i].addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					currentSquare = board[j];
					out.println("MOVE " + j);
				}
			});
			boardPanel.add(board[i]);
		}
		frame.getContentPane().add(boardPanel, "Center");
	}

	public void play() throws Exception {
		String response;
		try {
			response = in.readLine();
			if (response.startsWith("WELCOME")) {
				char mark = response.charAt(8);
				icon = new ImageIcon(mark == 'X' ? "x.gif" : "o.gif");
				opponentIcon = new ImageIcon(mark == 'X' ? "o.gif" : "x.gif");
				frame.setTitle("Jugador " + mark);
			}
			while (true) {
				response = in.readLine();
				if (response.startsWith("VALID_MOVE")) {
					messageLabel.setText("Esperando al otro jugaor");
					currentSquare.setIcon(icon);
					currentSquare.repaint();
				} else if (response.startsWith("OPPONENT_MOVED")) {
					int loc = Integer.parseInt(response.substring(15));
					board[loc].setIcon(opponentIcon);
					board[loc].repaint();
					messageLabel.setText("Oponente movi�, tu turno");
				} else if (response.startsWith("VICTORY")) {
					messageLabel.setText("Ganaste");
					win = 1;
					break;
				} else if (response.startsWith("DEFEAT")) {
					messageLabel.setText("Perdiste");
					win = 0;
					break;
				} else if (response.startsWith("TIE")) {
					messageLabel.setText("Empate");
					win = 2;
					break;
				} else if (response.startsWith("MESSAGE")) {
					messageLabel.setText(response.substring(8));
				}
			}
			out.println("QUIT");
		} finally {
			socket.close();
		}
	}

	public boolean wantsToPlayAgain() {
		int response = JOptionPane.showConfirmDialog(frame, "Jugar de nuevo?",
				":)", JOptionPane.YES_NO_OPTION);
		frame.dispose();
		return response == JOptionPane.YES_OPTION;
	}

	/**
	 * Cuadrados de la venta clientetictac. Cada cuadrado es un panel. los
	 * clientes llaman al metodo set icon para rellenarlos.
	 */
	static class Square extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		JLabel label = new JLabel((Icon) null);

		public Square() {
			setBackground(Color.white);
			add(label);
		}

		public void setIcon(Icon icon) {
			label.setIcon(icon);
		}
	}

	

}
