package Conexion;

import java.net.*;
import java.io.*;

import Datos.Usuario;
import Logica.Puntajes;

public class Cliente
{  private Socket socket              = null;
private DataInputStream  console   = null;
private DataOutputStream streamOut = null;
public String nombre;
private String pass;
public Puntajes puntaje;
private boolean es;
private Usuario user;

public Cliente(String nombre, String pass,Puntajes p)
{
	this.nombre = nombre;
	this.pass = pass;
	puntaje= p;
	es = false;
	user = new Usuario();
}


public Cliente(String nombre, String serverName,String pass, int serverPort,Puntajes p)

{  
	this.nombre = nombre;
	this.pass = pass;
	puntaje =p;
	es = true;
	System.out.println("Establishing connection. Please wait ...");
	try
	{  
		//Se ejecuta esta linea y se conecta al servidor (Ahora se esta coenctando al localHost)
		socket = new Socket(serverName, serverPort);
		System.out.println("Connected: " + socket);
		start();
	}
	catch(UnknownHostException uhe)
	{  System.out.println("Host unknown: " + uhe.getMessage());
	}
	catch(IOException ioe)
	{  System.out.println("Unexpected exception: " + ioe.getMessage());
	}



	try{ //Enviamos el nombre al servidor
		streamOut.writeUTF(nombre);
		streamOut.flush();
	}
	catch(IOException ioe)
	{  System.out.println("Sending error: " + ioe.getMessage());
	}

}
public void start() throws IOException
{  console   = new DataInputStream(System.in);
streamOut = new DataOutputStream(socket.getOutputStream());
}
public void stop()
{  try
{  if (console   != null)  console.close();
if (streamOut != null)  streamOut.close();
if (socket    != null)  socket.close();
}
catch(IOException ioe)
{  System.out.println("Error closing ...");
}
}
public void enviarpje()
{
	String [] juego= {"matematicas","scramble","memorice","gato"};
	if(es){
		for(int i=0;i<4;i++)
		{
			try{ //Enviamos el nombre al servidor
				String enviar= nombre+" puntaje "+juego[i]+" prom "+puntaje.puntajePromedio[i]+" min "+puntaje.puntajeMinimo[i]
						+" max "+puntaje.puntajeMaximo[i]+" cont "+puntaje.cantidad[i];
				streamOut.writeUTF(enviar);
				streamOut.flush();
			}
			catch(IOException ioe)
			{  System.out.println("Sending error: " + ioe.getMessage());
			}
		}
	}
	else
	{
		for(int i=0;i<4;i++)
		{
			//Enviamos el nombre al servidor
				String enviar= nombre+" puntaje "+juego[i]+" prom "+puntaje.puntajePromedio[i]+" min "+puntaje.puntajeMinimo[i]
						+" max "+puntaje.puntajeMaximo[i]+" cont "+puntaje.cantidad[i];
				user.registrarPuntaje(enviar);
			
		}
	}
}
}
