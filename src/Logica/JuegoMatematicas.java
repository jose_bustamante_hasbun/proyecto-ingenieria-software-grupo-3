package Logica;

public class JuegoMatematicas {
	public int inicial;
	public int fin;
	public int resultado1;
	public int resultado2;
	public int aleatoriosigno;
	public String s="";

	public JuegoMatematicas() {

	}

	public void jugar() {
		generar();
		if (!revisar()) {
			jugar();
		}
		System.out.println(inicial + " " + aleatoriosigno + " " + fin + " = "
				+ resultado1 + resultado2);
	}

	public String imprimir() {
		String rs1 = "";
		String a = "";
		if (aleatoriosigno == 1) {
			s = "+";
			// isigno = new Imagen(100, 150,"suma");
		}
		if (aleatoriosigno == 2) {
			s = "-";
			// isigno = new Imagen(100, 150, "resta");
		}
		if (aleatoriosigno == 3) {
			s = "x";
			// isigno = new Imagen(100, 150, "multiplicacion");
		}
		if (aleatoriosigno == 4) {
			s = "/";
			// isigno = new Imagen(100, 150, "division");
		}
		if (resultado1 == -1) {
			rs1 = "-";
			a = inicial + " " + "  ?  " + " " + fin + "  =  " + rs1
					+ resultado2;
		} else
			a = inicial + " " + "  ?  " + " " + fin + "  =  " + resultado1
			+ resultado2;
		return a;
	}

	public void generar() {
		inicial = generarNumeroAleatorio(10);
		fin = generarNumeroAleatorio(10);
		aleatoriosigno = generarNumeroAleatorio(4);
		resultado1 = generarResultado()[0];
		resultado2 = generarResultado()[1];
	}

	public String devolverSigno() {
		return s;
	}

	private int[] generarResultado() {
		int aux1 = 0, aux2 = 0;
		if (aleatoriosigno == 1) {
			aux2 = inicial + fin;
			if (aux2 >= 10) {
				aux2 = aux2 - 10;
				aux1 = 1;
			}
		} else if (aleatoriosigno == 2) {
			aux2 = inicial - fin;
			if (aux2 < 0) {
				aux1 = -1;
				aux2 = Math.abs(inicial - fin);
			}
		} else if (aleatoriosigno == 3) {
			aux2 = inicial * fin;
			if (aux2 > 10) {
				int a = aux2 / 10;
				aux2 = aux2 - a * 10;
				aux1 = a;
			}

		} else {
			aux2 = inicial / fin;
		}

		int[] arreglo = new int[2];
		arreglo[0] = aux1;
		arreglo[1] = aux2;
		return arreglo;
	}

	private int generarNumeroAleatorio(int n) {
		int aux = (int) Math.floor(Math.random() * n) + 1;
		while (aux == 10)
			aux = (int) Math.floor(Math.random() * n) + 1;
		return aux;
	}

	private boolean revisar() {
		boolean t;
		t = true;
		if (fin == 1 && aleatoriosigno > 2)
			t = false;
		if (inicial == 2 && fin == 2
				&& (aleatoriosigno == 1 || aleatoriosigno == 3))
			t = false;
		if (inicial == 4 && fin == 2
				&& (aleatoriosigno == 4 || aleatoriosigno == 2))
			t = false;
		if (inicial < fin && aleatoriosigno == 4)
			t = false;
		return t;
	}

	
	public  String CambiarOperacion(String s)
	{
		String aux="";
		if(s.equals("q"))
			aux="+";
		if(s.equals("w"))
			aux="-";
		if(s.equals("e"))
			aux="x";
		if(s.equals("r"))
			aux="/";
		return aux;
	}
}
