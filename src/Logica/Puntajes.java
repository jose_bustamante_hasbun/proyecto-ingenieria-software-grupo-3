package Logica;

public class Puntajes {
	
	public int[] puntajeMaximo;
	public int[] puntajeMinimo;
	public int[] puntajePromedio;
	public int[] cantidad; 
	
	public Puntajes(int[] mat, int []mem, int [] scra, int[] gato)
	{
		puntajePromedio= new int[4];
		puntajeMaximo= new int[4];
		puntajeMinimo= new int[4];
		cantidad = new int [4];
		cantidad[0]=mat[3];
		cantidad[1]=mem[3];
		cantidad[2]=scra[3];
		cantidad[3]=gato[3];
		puntajePromedio[0]=mat[0];
		puntajePromedio[1]=mem[0];
		puntajePromedio[2]=scra[0];
		puntajePromedio[3]=gato[0];
		puntajeMinimo[0]=mat[1];
		puntajeMinimo[1]=mem[1];
		puntajeMinimo[2]=scra[1];
		puntajeMinimo[3]=gato[1];
		puntajeMaximo[0]=mat[2];
		puntajeMaximo[1]=mem[2];
		puntajeMaximo[2]=scra[2];
		puntajeMaximo[3]=gato[2];
	}

	public void setPuntajeMatematica(int pje)
	{
		cantidad[0]++;
		if(cantidad[0]==1)
		{
			puntajeMaximo[0]=pje;
			puntajeMinimo[0]=pje;
		}
		if(pje> puntajeMaximo[0])
		{
			puntajeMaximo[0]= pje;
		}
		if(pje< puntajeMinimo[0])
		{
			puntajeMinimo[0]= pje;
		}
		puntajePromedio[0]=((puntajePromedio[0]*(cantidad[0]-1))+ pje)/cantidad[0];
	}
	public void setPuntajeScramble(int pje)
	{
		cantidad[1]++;
		if(cantidad[1]==1)
		{
			puntajeMaximo[1]=pje;
			puntajeMinimo[1]=pje;
		}
		if(pje> puntajeMaximo[1])
		{
			puntajeMaximo[1]= pje;
		}
		if(pje< puntajeMinimo[1])
		{
			puntajeMinimo[1]= pje;
		}
		puntajePromedio[1]=((puntajePromedio[1]*(cantidad[1]-1))+ pje)/cantidad[1];
	}
	public void setPuntajeMemorice(int pje)
	{
		cantidad[2]++;
		if(cantidad[2]==1)
		{
			puntajeMaximo[2]=pje;
			puntajeMinimo[2]=pje;
		}
		if(pje> puntajeMaximo[2])
		{
			puntajeMaximo[2]= pje;
		}
		if(pje< puntajeMinimo[2])
		{
			puntajeMinimo[2]= pje;
		}
		puntajePromedio[2]=((puntajePromedio[2]*(cantidad[2]-1))+ pje)/cantidad[2];
	}
	public void setPuntajeGato(int pje)
	{
		cantidad[3]++;
		if(cantidad[3]==1)
		{
			puntajeMaximo[3]=pje;
			puntajeMinimo[3]=pje;
		}
		if(pje> puntajeMaximo[3])
		{
			puntajeMaximo[3]= pje;
		}
		if(pje< puntajeMinimo[3])
		{
			puntajeMinimo[3]= pje;
		}
		puntajePromedio[3]=((puntajePromedio[3]*(cantidad[3]-1))+ pje)/cantidad[3];
	}
}
