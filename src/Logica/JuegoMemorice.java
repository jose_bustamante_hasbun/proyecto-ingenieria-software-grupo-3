package Logica;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

import static java.util.Collections.*;

public class JuegoMemorice {

	public ArrayList<Integer> gameList = new ArrayList<Integer>();
	private Tiempo timer;

	public JuegoMemorice() {
		timer = new Tiempo();
		timer.Contar();
		
	}
	
	public JButton[] setArrayListText(JButton[] gameBtn) {
		// Creamos los numeros y los metemos a una lista.
		for (int i = 0; i < 2; i++) {
			for (int ii = 1; ii < (gameBtn.length / 2) + 1; ii++) {
				gameList.add(ii);
			}
		}
		// Re ordenamos la lista
		shuffle(gameList);

		// ///////////////////
		int newLine = 0;
		for (int a = 0; a < gameList.size(); a++) {
			newLine++;
			if (newLine == 4) {
				newLine = 0;
			}
		}
		return gameBtn;
	}

	

	public int contador() {
		int segundos;
		segundos = timer.getSegundos();
		return segundos;
	}
}
