package Logica;

import javax.swing.*;

import java.awt.event.*;
import java.util.*;
import java.io.*;

public class JuegoScramble {

	private String palabra;
	private String listapalabras[] = new String[79218];
	private int numpalabras = 0;
	private char[] abc = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			'v', 'w', 'x', 'y', 'z' };
	private char[] vocales = new char[]{'a','e','i','o','u'};
	private List<String> usadas;

	public JuegoScramble() {
		usadas = new ArrayList<String>();
		try {

			BufferedReader in = new BufferedReader(new FileReader(
					"palabras.txt"));
			String str;
			while ((str = in.readLine()) != null) {
				listapalabras[numpalabras++] = str;
			}
			in.close();
		} catch (IOException e) {
		}
	}


	// metodo que revisa si existe la palabra
	public boolean buscarPalabra(String palabraJugada, String palabra) {
		boolean estaEnLista = false;
		boolean estaUsada = false;
		boolean contiene = false;
		palabra=palabraSinGuion(palabra);
		for (String i : listapalabras)
			if (palabraJugada.equalsIgnoreCase(i))
				estaEnLista = true;
		for (String i : usadas) {
			if (palabraJugada.equals(i))
				estaUsada = true;
		}
		char[] aux1 = palabraJugada.toCharArray();
		char[] aux2 = palabra.toCharArray();
		boolean[] ver = new boolean[aux1.length];
		for (int a = 0; a < ver.length; a++)
			for (char b : aux2)
				if (aux1[a] == b)
					ver[a] = true;
		contiene = true;
		for (boolean a : ver)
			contiene = contiene && a;
		if(estaEnLista && !estaUsada && contiene)
		{
			usadas.add(palabraJugada);
			return true;
		}
		else
			return false;
	}

	public String palabraSinGuion(String s)
	{
		String aux="";
		for(int i=0;i<11;i++)
		{
			if(i%2==0)
			{
				aux+=s.charAt(i);
			}	
		}
		return aux;
	}
	// retorna todas las palabras encontradas separadas por un gui�n
	public String getPalabras() {
		String aux = "";
		for (String i : usadas)
			aux += i + "  ";
		return aux;
	}

	// retorna un arreglo con letras
	public String getLetras() {
		char[] aux = new char[6];
		Random rand = new Random();
		int contador = 0;
		while (true) {
			if (contador >= 4)
			{
				int i = rand.nextInt(vocales.length);
				char a = vocales[i];
				for (char b : aux)
					if (b == a)
						continue;
				aux[contador] = a;
				contador++;
			}
			else
			{
				int i = rand.nextInt(abc.length);
				char a = abc[i];
				for (char b : aux)
					if (b == a)
						continue;
				aux[contador] = a;
				contador++;
			}
			if(contador >= 6)
				break;
		}
		String s="";
		for (int i=0; i<6;i++) {
			if(i<5)
				s+=aux[i]+"-";
			else
				s+=aux[i];
		}
		return s;
	}
}
