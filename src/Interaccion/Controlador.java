package Interaccion;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Conexion.Cliente;
import Logica.Tiempo;

public abstract class Controlador extends JFrame {
	// clase comun entre los juegos
	
	public Cliente cliente;
	public Tiempo timer;
	public int puntaje;
	
	public Controlador(Cliente c)
	{
		cliente=c;
		this.setFocusable(true);
		setTitle("Bobugama - Jugador:" + cliente.nombre);
		setSize(700, 500);
		setVisible(true);
		timer = new Tiempo();
		empezar();
	}
	public Controlador(Cliente c, String s,int puntajeT,int juego)
	{
		cliente = c;
		JOptionPane.showMessageDialog(this, s);
		timer= new Tiempo();
		puntaje=puntajeT;
		empezar();
		if(juego==0)
			cliente.puntaje.setPuntajeMatematica(puntajeT);
		if(juego==1)
			cliente.puntaje.setPuntajeMemorice(puntajeT);
		if(juego==2)
			cliente.puntaje.setPuntajeScramble(puntajeT);
		if(juego==3)
		{
			cliente.puntaje.setPuntajeGato(puntajeT);
		}
		cliente.enviarpje(); 	
	}
	public abstract void empezar();
}
