package interfaz;


import java.awt.event.*;
import java.awt.*;

import javax.swing.*;

public class VentanaSC extends JFrame implements ActionListener, WindowListener{
	
	private JButton btnS;
	private JButton btnC;
	
	public VentanaSC(){
		
		Container cp = getContentPane();
		cp.setLayout(new GridLayout(2,2));//izquiera a derecha
		
		btnS= new JButton( new AbstractAction("Servidor") {
	        @Override
	        public void actionPerformed( ActionEvent e ) {
	            VentanaServidor VS = new VentanaServidor();
	            setVisible(false);
	        }
	    });
		btnC= new JButton( new AbstractAction("Cliente") {
	        @Override
	        public void actionPerformed( ActionEvent e ) {
	            VentanaInicio vi = new VentanaInicio(false);
	            
	            setVisible(false);
	        }
	    });
		
		add(btnS);
		add(btnC);
	
	
		btnS.addActionListener(this);
		btnC.addActionListener(this);
		addWindowListener(this);
		
		setTitle("Bobugama");
		setSize(500, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		System.out.println();
		
	}
	

}
