package interfaz;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;

import Conexion.Cliente;
import Interaccion.Controlador;
import Logica.JuegoMemorice;

public class SelectorJuego extends Controlador implements ActionListener, WindowListener
{
		private JButton JuegoMatematicas,JuegoScrumble,JuegoMem,Juego4,Usuario;
		private JPanel PUsuario,gamePnl;
		public SelectorJuego(Cliente c) {
			super(c);
			// TODO Auto-generated constructor stub
		}
		
		public SelectorJuego(Cliente c, String s,int puntajeT,int juego)
		{
			super(c,s,puntajeT,juego);
		}
		
		@Override
		public void empezar() 
		{//izquiera a derecha
		    
		   
			JuegoScrumble = new JButton( new AbstractAction("JuegoScrumble") {
		        @Override
		        public void actionPerformed( ActionEvent e ) {
		        	//Llamar ventana juevo scrumble
		        	VentanaScramble vs=new VentanaScramble(cliente);
		        	setVisible(false);
		        }
		    });
			Juego4 = new JButton(new AbstractAction("TicTac") {
		        @Override
		        public void actionPerformed( ActionEvent e ) {
		        	//Llamar ventana juego4
		        	
		        	setVisible(false);
		        	
		        	VentanaTicTac vt = new VentanaTicTac(cliente);
		        	
		        	
		        }
		    });
			
			JuegoMatematicas = new JButton(new AbstractAction("JuegoMatematicas") {
		        @Override
		        public void actionPerformed( ActionEvent e ) {
		        	VentanaMatematicas v= new VentanaMatematicas(cliente);
		        	setVisible(false);
		        }
		    });

		    JuegoMem = new JButton( new AbstractAction("JuegoMemorice") { 
		        @Override
		        public void actionPerformed( ActionEvent e ) {
		            VentanaMemorice jm = new VentanaMemorice(cliente);
		            setVisible(false);
		        }
		    });
			
			setTitle("Bobugama, Jugador: "+cliente.nombre);
			setSize(700, 500);
			setVisible(true);
			
			// parte nueva con la informacion del usuario
			
			Usuario = new JButton(new AbstractAction("Usuario") {
		        @Override
		        public void actionPerformed( ActionEvent e ) {
		        	VentanaUsuario vu= new VentanaUsuario(cliente);
		        	setVisible(false);
		        }
		    });;
		    
		    gamePnl= new JPanel();
			gamePnl.setLayout(new GridLayout(2, 2));
		    gamePnl.add(JuegoMatematicas);
		    gamePnl.add(JuegoMem);
		    gamePnl.add(JuegoScrumble);
		    gamePnl.add(Juego4);
			addWindowListener(this);
		    Panel buttonPnl = new Panel();
			buttonPnl.setLayout(new GridLayout(0, 1));
			buttonPnl.add(Usuario);
			
		    add(gamePnl, BorderLayout.CENTER);
			add(buttonPnl, BorderLayout.SOUTH);
		}
		@Override
		   public void actionPerformed(ActionEvent evt) {
			
						
		      
		   }
		 
		 @Override
		   public void windowClosing(WindowEvent e) {
		      System.exit(0);  // terminate the program
		   }
		 
		   @Override
		   public void windowOpened(WindowEvent e) { }
		   @Override
		   public void windowClosed(WindowEvent e) { }
		   @Override
		   public void windowIconified(WindowEvent e) { }
		   @Override
		   public void windowDeiconified(WindowEvent e) { }
		   @Override
		   public void windowActivated(WindowEvent e) { }
		   @Override
		   public void windowDeactivated(WindowEvent e) { }
}
