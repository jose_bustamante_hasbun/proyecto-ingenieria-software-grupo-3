package interfaz;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import Conexion.Cliente;
import Interaccion.Controlador;

public class VentanaUsuario extends Controlador {


	private JLabel Jnombre;
	private JTable  Jtabla;
	private JButton Jvolver;
	
	private JTextArea Jamigos;
	private BufferedImage img;
	private JPanel Jmid;
	private JPanel Jsur;
	private Object[][] datos;
	private Cliente c;
	private JPanel JmidDerecha = new JPanel();
	private JLabel JImagen;

	public VentanaUsuario(Cliente c){
		super(c);
		this.c = c;

		// TODO Auto-generated constructor stub
	}

	@Override
	public void empezar() {


		


		Jnombre = new JLabel("Nombre: "+cliente.nombre);
		Jvolver = new JButton( new AbstractAction("Volver") {
			@Override
			public void actionPerformed( ActionEvent e ) {
				setVisible(false);
				SelectorJuego j = new SelectorJuego(c);
			}
		});
		Jmid= new JPanel();
		JmidDerecha = new JPanel();
		JmidDerecha.setLayout(new GridLayout(2,0));
		Jmid.setLayout(new GridLayout(0,2)); 
		Jmid.add(JmidDerecha);
		Jsur = new JPanel();
		Jsur.add(Jvolver);
		add(Jnombre, BorderLayout.NORTH);
		add(Jmid, BorderLayout.CENTER);
		add(Jsur, BorderLayout.SOUTH);
		crearTabla();
		agregarAmigos();



	}

	public void crearTabla()
	{
		Object[][] data = {
				{"Juego Matematica", cliente.puntaje.puntajeMinimo[0],
					cliente.puntaje.puntajePromedio[0], cliente.puntaje.puntajeMaximo[0]},
					{"Juego Scrumble", cliente.puntaje.puntajeMinimo[1],
						cliente.puntaje.puntajePromedio[1], cliente.puntaje.puntajeMaximo[1]},
						{"Juego Memorice", cliente.puntaje.puntajeMinimo[2],
							cliente.puntaje.puntajePromedio[2], cliente.puntaje.puntajeMaximo[2]},
							{"Gato", cliente.puntaje.puntajeMinimo[3],
								cliente.puntaje.puntajePromedio[3], cliente.puntaje.puntajeMaximo[3]},
		};
		String[] columnNames = {"Juego", "Puntaje Minimo",
				"Puntaje Promedio",
		"Maximo"};
		JTable table = new JTable(data,columnNames);
		//Jmid.add(table);
		JmidDerecha.add(table);

	}
	public void loadImage()
	{

		FileDialog fd = new FileDialog(new Frame(), "Please choose a file:", FileDialog.LOAD);
		fd.show();
		System.out.println(fd.getDirectory());
		if (fd.getFile() != null)
		{
			try {
				img = ImageIO.read(new File(fd.getDirectory()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JImagen = new JLabel(new ImageIcon(img));
			JmidDerecha.add(JImagen);
		}

	}
	public void agregarAmigos(){
		
		File f = new File("C:\\ProyectoCasiFinal");
		System.out.println("hola");
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.startsWith("temp") && name.endsWith("txt");
		    }
		});	
	}


}
