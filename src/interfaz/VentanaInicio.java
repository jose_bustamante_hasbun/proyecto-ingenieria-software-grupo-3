package interfaz;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;

import Conexion.Cliente;
import Datos.Usuario;
import Logica.Puntajes;

public class VentanaInicio extends JFrame implements ActionListener,
		WindowListener {
	private JLabel usuario;
	private JLabel contrasena, contrasena2;
	private JTextField CompletarUsuario;
	private JPasswordField CompletarContrasena, completarConstrasena2;
	private JButton go, nuevoUsuario;
	private String Scontrasena1, Scontrasena2;
	private Usuario creadorUsuario; 
	private boolean validador;
	private Puntajes puntaje;
	public VentanaInicio(boolean validador) {
		
		this.validador=validador;		
		empezar();
	
	}

	public void empezar()
	{
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		Panel p1 = new Panel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.LINE_AXIS));
		Panel p2 = new Panel();
		p2.setLayout(new BoxLayout(p2, BoxLayout.LINE_AXIS));// izquiera a
		creadorUsuario = new Usuario();														// derecha
		usuario = new JLabel("usuario: ");
		contrasena = new JLabel("              contrasena:");
		CompletarUsuario = new JTextField(20);
		CompletarContrasena = new JPasswordField(20);// creamos los objetos
		nuevoUsuario = new JButton("Crear cuenta");
		go = new JButton("Entrar");
		p1.add(usuario);
		p1.add(CompletarUsuario);
		p1.add(go);
		p2.add(contrasena);
		p2.add(CompletarContrasena);
		p2.add(nuevoUsuario);// agregamos a la ventana

		go.addActionListener(this);
		nuevoUsuario.addActionListener(this);
		addWindowListener(this);

		setTitle("Bobugama");
		setSize(500, 150);
		setVisible(true);
		add(p1);
		add(p2);
		completarConstrasena2 = new JPasswordField(20);
		contrasena2 = new JLabel("reingrese clave	");
	}
	@Override
	public void actionPerformed(ActionEvent evt) {

		String btnLabel = evt.getActionCommand();
		if (btnLabel.equals("Entrar")) {
			String nombreUsuario = CompletarUsuario.getText();
			String nombreContrasena = CompletarContrasena.getText();
			if (nombreContrasena.equals("") || nombreUsuario.equals("")) {
				JOptionPane.showMessageDialog(this, "Ingresa los datos",
						"error", JOptionPane.ERROR_MESSAGE);

			}
			else if(creadorUsuario.leerArchivo(nombreUsuario, nombreContrasena))
			{
				if(!validador)
				{
					try
					{
					setVisible(false);
					Cliente c = new Cliente(nombreUsuario, "localhost",
							nombreContrasena, 5555,creadorUsuario.puntajesUsuario(nombreUsuario));
					SelectorJuego selector = new SelectorJuego(c);
					}
					catch(Exception e)
					{
						JOptionPane.showMessageDialog(this, "Servidor fuera de linea",
								"error", JOptionPane.ERROR_MESSAGE);
					}
				}
				else
				{
					setVisible(false);

					Cliente c = new Cliente(nombreUsuario,nombreContrasena,creadorUsuario.puntajesUsuario(nombreUsuario));
					SelectorJuego selector = new SelectorJuego(c);
				}
			}
			else if(!creadorUsuario.leerArchivo(nombreUsuario, nombreContrasena))
			{
				JOptionPane.showMessageDialog(this, "Usuario o contraseña mal ingresado",
						"error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if (btnLabel.equals("Crear cuenta")) {
			Panel p3 = new Panel();
			p3.setLayout(new BoxLayout(p3, BoxLayout.LINE_AXIS));
			p3.add(completarConstrasena2);
			add(contrasena2);
			add(p3);
			nuevoUsuario.setText("Crear");
			go.setVisible(false);

		}
		if(btnLabel.equals("Crear"))
		{
			Scontrasena1=CompletarContrasena.getText();
			Scontrasena2=completarConstrasena2.getText();
			int largo=Scontrasena1.length();
			// aca se crea el usuario como un texto con su informacion
			if(Scontrasena1.equals(Scontrasena2) && !CompletarUsuario.getText().equals("") && !creadorUsuario.existeUsuario(CompletarUsuario.getText()))
			{
				JOptionPane.showMessageDialog(this, "Usuario: "+ CompletarUsuario.getText()+", creado satisfactoriamente",
						"", JOptionPane.YES_OPTION);
				creadorUsuario.crearUsuario(CompletarUsuario.getText(), Scontrasena1);
				completarConstrasena2.setVisible(false);
				contrasena2.setVisible(false);
				go.setVisible(true);
				nuevoUsuario.setVisible(false);
				go.setText("Entrar");
			}
			else if(largo<6)
				JOptionPane.showMessageDialog(this, "ingrese una clave de mas de 6 digitos",
						"error", JOptionPane.YES_OPTION);
			
			else if(creadorUsuario.existeUsuario(CompletarUsuario.getText()))
				JOptionPane.showMessageDialog(this, "Ya existe un usuario con ese nombre.\b reintente con otro nombre de usuario",
						"error", JOptionPane.YES_OPTION);
			else
				JOptionPane.showMessageDialog(this, "reingrese valores validos",
						"error", JOptionPane.YES_OPTION);
		}

	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0); // terminate the program
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}
