package interfaz;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.TextEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Bobugama.*;
import Conexion.Cliente;
import Interaccion.Controlador;
import Logica.JuegoScramble;
import Logica.Tiempo;

public class VentanaScramble extends Controlador implements ActionListener,
		WindowListener
{
	public VentanaScramble(Cliente c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	// de lo que saque de matematicas
	
	private JButton aceptar, volver, scramble;
	private JuegoScramble juego;
	private JLabel palabra, tiemporestante,palabrasEncontradas;
	private JTextField tfCount;
	private int count = 0, veces = 0, segundos = 0;
	private Tiempo timer;
	private int puntaje;
	// de lo q saque de scramble
	JPanel wsPanel;
	private JButton wsButton1;
	private JTextField FieldLlenado;
	private boolean validador;
	
	// gestion de ventana
	public void	 actionPerformed(ActionEvent event) {
		if (event.getSource() == wsButton1) {
			String text = FieldLlenado.getText();
			if (text.length() == 0) {
				JOptionPane.showMessageDialog(this, "Debes introducir algo!");
			} else if (juego.buscarPalabra(text,palabra.getText())) {
				JOptionPane.showMessageDialog(this, "Correcto!");
				palabrasEncontradas.setText("palabras encontradas: "+juego.getPalabras());
				
			} else {
				JOptionPane.showMessageDialog(this, "Mal!");
			}
			FieldLlenado.setText("");
			FieldLlenado.requestFocus();
		}
		if(event.getSource()==volver)
		{
			setVisible(false);
			SelectorJuego selector = new SelectorJuego(cliente);
			validador=false;
		}
		if (event.getSource() == scramble)
		{
			palabra.setText( juego.getLetras());
		}
	}

	// /////////////////////////////////////////////////////////////////7

	public void empezar() {

		// se carga la lista de palabras
				juego = new JuegoScramble();
				timer = new Tiempo();
				puntaje=0;
				palabra= new JLabel();
				wsButton1 = new JButton("Aceptar");
				FieldLlenado = new JTextField("Introdusca la palabra");
				addWindowListener(this);
				volver = new JButton("Volver");
				palabrasEncontradas = new JLabel("palabras encontradas: ");
				scramble = new JButton("scramble");
				BtnListener listener = new BtnListener();
				validador=true;
				volver.addActionListener(this);
				juego = new JuegoScramble();
				tfCount = new JTextField("Puntaje Actual: 0", 10);
				tfCount.setFont(new Font("Arial", Font.PLAIN, 35));
				tfCount.setEditable(false);
				this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				this.setLocationRelativeTo(null);
				JPanel wsPanel = new JPanel();
				wsPanel.setLayout(new GridLayout(5,1));
				FieldLlenado = new JTextField(13);
				wsButton1 = new JButton("Adivina!");
				wsButton1.addActionListener(this);
				scramble.addActionListener(this);
				this.setVisible(true);
				FieldLlenado.requestFocus();

				FieldLlenado.addKeyListener(
			            new KeyListener(){

			                public void keyPressed(KeyEvent e){

			                    if(e.getKeyCode() == KeyEvent.VK_ENTER)
			                    {
			                    	String text = FieldLlenado.getText();
			            			if (text.length() == 0) {
			            				//JOptionPane.showMessageDialog(this, "Debes introducir algo!");
			            			} else if (juego.buscarPalabra(text,palabra.getText())) {
			            				//JOptionPane.showMessageDialog(this, "Correcto!");
			            				palabrasEncontradas.setText("palabras encontradas: "+juego.getPalabras());
			            				puntaje+=4;
			            				tfCount.setText("Puntaje Actual: "+puntaje);
			            				
			            			} else {
			            				//JOptionPane.showMessageDialog(this, "Mal!");
			            			}
			            			FieldLlenado.setText("");
			            			FieldLlenado.requestFocus();
			                    }     
			                    if(e.getKeyCode()==KeyEvent.VK_SPACE)
			                    {
			            			palabra.setText( juego.getLetras());
			            			FieldLlenado.setText("");
			            			FieldLlenado.requestFocus();
		            				puntaje-=1;
			            			tfCount.setText("Puntaje Actual: "+puntaje);
			                    }
			                }

							@Override
							public void keyReleased(KeyEvent e) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void keyTyped(KeyEvent e) {
								// TODO Auto-generated method stub
								
							}
			            });
				timer.Contar();
		Container cp = getContentPane();
		cp.setLayout(new GridLayout(6, 1));
		palabra = new JLabel(juego.getLetras(),
				palabra.CENTER);
		palabra.setFont(new Font("Arial", Font.PLAIN, 40));
		tiemporestante = new JLabel("Tiempo restante: " + contador());
		tiemporestante.setFont(new Font("Arial", Font.PLAIN, 35));
		add(tiemporestante);
		add(tfCount);
		add(palabra);
		JPanel j=new JPanel();
		j.setLayout(new FlowLayout());
		add(j);
		j.add(FieldLlenado);
		j.add(wsButton1);
		j.add(scramble);
		add(j);
		add(palabrasEncontradas);
		add(volver);
		new javax.swing.Timer(1, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (contador() > 0) {
					tiemporestante.setText("Tiempo Restante: " + contador());
				}
				if (contador() <= 0 && validador) {
					validador = false;
					tiemporestante.setText("fin del juego");
					setVisible(false);
					SelectorJuego selector = new SelectorJuego(cliente,
							"Fin de juego, puntaje: " + puntaje,puntaje,2);
					// actualizar info del jugador
					// pje actual, pje promedio, pje maximo
				}
			}
		}).start();// metodo que no se puede sacar de este lugar ya que es un
					// evento por tiempo
	}

	public void actionPerfomed(TextEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0); // terminate the program
	}

	@Override
	public void windowOpened(WindowEvent e) {
		JOptionPane
				.showMessageDialog(
						this,
						"Encuentra palabras con este set de letras, \n presiona enter para probar y \n la barra espaciadora para mezclar las letras");
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	private class BtnListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			//activar boton listener de aceptado
		}
	}

	public int gettiemporestante() {
		return count;
	}

	public int contador() {
		segundos = 90 - timer.getSegundos();
		return segundos;
	}

}
