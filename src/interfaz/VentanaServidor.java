package interfaz;

import java.awt.event.*;
import java.awt.*;
import java.net.*;
import java.util.Enumeration;

import javax.swing.*;

import Conexion.Servidor;

public class VentanaServidor extends JFrame implements ActionListener, WindowListener{

	private JLabel IP;
	private JButton conectar;
	private static JProgressBar barra;
	private static JFrame ventana;
	private Enumeration<NetworkInterface> e;
	
	
	public VentanaServidor() {

		ventana = new JFrame("Cargando");
		conectar= new JButton( new AbstractAction("Conectar") {
	        @Override
	        public void actionPerformed( ActionEvent e ) {
	        	new Thread(new Hilo()).start();
				ventana.setVisible(true);
	            setVisible(false);
	        }
	    });
		barra = new JProgressBar(0,100);
		ventana.setLayout(new GridLayout(2,2));
		ventana.add(barra);
		ventana.add(conectar);
		ventana.pack();
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		add(conectar);
		addWindowListener(this);
		setTitle("Bobugama");
		setSize(500, 300);
		setVisible(true);



	}


	
	public static class Hilo implements Runnable{
		
		public void run(){
			//Cuando termina de cargar la barra se abre Esperanco clientes
			for(int i=0;i<=100;i++){
				
				barra.setValue(i);
				barra.repaint();
				
				if(i==100){
					
					ventana.setVisible(false);
					Servidor s = new Servidor();
					
					
				}
				try{
					Thread.sleep(50);
				}
				catch(Exception ex){
					
				}
				
			}
			
		}
		
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
