package interfaz;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

import Conexion.Cliente;
import Interaccion.Controlador;
import Logica.JuegoMemorice;
import static java.util.Collections.*;

public class VentanaMemorice extends Controlador implements ActionListener {

	public VentanaMemorice(Cliente c) {
		super(c);
		init();
		panel();
		memo = new JuegoMemorice();
		gameBtn = memo.setArrayListText(gameBtn);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// TODO Auto-generated constructor stub
	}

	public void OnFinish() {
		boolean aux = true;
		for (JButton i : gameBtn)
			aux = aux && !i.getText().equals("");
		if (aux){			
			setVisible(false);
			SelectorJuego selector = new SelectorJuego(cliente,
				"El tiempo	 fue de " + memo.contador() +", y el puntaje fue de "+obtenerPuntaje(), obtenerPuntaje(),1);
			//aca va lo q pasa al final
			}
	}

	private static final long serialVersionUID = 1L;
	private JButton exitBtn, volverBtn; // replayBtn;
	private JButton[] gameBtn = new JButton[16];
	private JLabel tiempo;
	private int counter = 0;
	private int[] btnID = new int[2];
	private int[] btnValue = new int[2];
	JuegoMemorice memo;

	@Override
	public void empezar() {
		// TODO Auto-generated method stub
	}

	// public VentanaMemorice(Cliente j) {
	// init();
	//
	// }

	public void init() {
		// Se rellena la ventana con botones
		gameBtn = new JButton[16];
		btnID = new int[2];
		btnValue = new int[2];
		for (int i = 0; i < gameBtn.length; i++) {
			gameBtn[i] = new JButton();
			gameBtn[i].setFont(new Font("Serif", Font.BOLD, 28));
			gameBtn[i].addActionListener(this);
		}
		// Creamos boton exit
		exitBtn = new JButton("Exit");
		volverBtn = new JButton("Volver");
		tiempo = new JLabel("Tiempo restante: ");
		exitBtn.addActionListener(this);
		volverBtn.addActionListener(this);

	}

	public void panel() {
		// Creamos el panel
		Panel gamePnl = new Panel();
		gamePnl.setLayout(new GridLayout(4, 4));
		// Agregamos los botones al panel
		for (int i = 0; i < gameBtn.length; i++) {
			gamePnl.add(gameBtn[i]);
		}

		// Agregamso boton exit y replay
		Panel buttonPnl = new Panel();
		// buttonPnl.add(replayBtn);
		buttonPnl.setLayout(new GridLayout(0, 3));
		buttonPnl.add(exitBtn);
		buttonPnl.add(volverBtn);
		add(gamePnl, BorderLayout.CENTER);
		add(buttonPnl, BorderLayout.SOUTH);
		buttonPnl.add(tiempo);
		new javax.swing.Timer(1, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (memo.contador() > 0)
					tiempo.setText("Tiempo Actual: " + memo.contador());
			}
		}).start();
	}

	public boolean sameValues() {
		if (btnValue[0] == btnValue[1]) {
			return true;
		}
		return false;
	}

	private int obtenerPuntaje() {
		return 500 / memo.contador();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Logica del juego
		if (exitBtn == e.getSource()) {
			System.exit(0);
		}
		if (volverBtn == e.getSource()) {
			setVisible(false);
			SelectorJuego selector = new SelectorJuego(cliente);
		}

		for (int i = 0; i < gameBtn.length; i++) {
			if (gameBtn[i] == e.getSource()) {

				gameBtn[i].setText("" + memo.gameList.get(i));
				gameBtn[i].setEnabled(false);
				counter++;
				if (counter == 3) {
					if (sameValues()) {
						gameBtn[btnID[0]].setEnabled(false);
						gameBtn[btnID[1]].setEnabled(false);
					} else {
						gameBtn[btnID[0]].setEnabled(true);
						gameBtn[btnID[0]].setText("");
						gameBtn[btnID[1]].setEnabled(true);
						gameBtn[btnID[1]].setText("");
					}
					counter = 1;
				}
				if (counter == 1) {
					btnID[0] = i;
					btnValue[0] = memo.gameList.get(i);
				}
				if (counter == 2) {
					btnID[1] = i;
					btnValue[1] = memo.gameList.get(i);
				}
			}
		}
		OnFinish();
	}

}
