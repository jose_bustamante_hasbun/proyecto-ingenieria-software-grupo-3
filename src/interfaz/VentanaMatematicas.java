package interfaz;

import java.awt.Button;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.TextEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.*;

import Conexion.Cliente;
import Interaccion.Controlador;
import Logica.JuegoMatematicas;
import Logica.Tiempo;

public class VentanaMatematicas extends Controlador implements ActionListener,
		WindowListener {
	public VentanaMatematicas(Cliente c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	private boolean validador;
	private JButton atras, suma, resta, division, multiplicacion, volver;
	private JuegoMatematicas juego = new JuegoMatematicas();
	private JLabel todo, tiemporestante;
	private JTextField tfCount;
	private int count = 0, veces = 0, segundos = 0; 
	@Override
	public void empezar() {
		
		validador = true;
		atras = new JButton("atras");
		atras.addActionListener(this);
		addWindowListener(this);
		suma = new JButton("+");
		resta = new JButton("-");
		division = new JButton("/");
		multiplicacion = new JButton("x");
		volver = new JButton("Volver");
		BtnListener listener = new BtnListener();
		// Use the same listener to all the 4 buttons.
		suma.addActionListener(listener);
		resta.addActionListener(listener);
		division.addActionListener(listener);
		multiplicacion.addActionListener(listener);
		volver.addActionListener(listener);
		this.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent e) {
				char ch = e.getKeyChar();
				veces++;
				String s = "" + ch;
				if (s.equals("q") || s.equals("w") || s.equals("e")
						|| s.equals("r")) {
					s = juego.CambiarOperacion(s);
					if (veces == 1) {
						timer.Contar();
					}
					if (s.equals(juego.devolverSigno())) {
						count += 4;
					} else
						count--;
					tfCount.setText("Puuntaje actual: " + count
							+ "          Intentos: " + veces);
					juego.generar();
					todo.setText(juego.imprimir());
				}
			}

			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
			}


			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
			}
		});

		juego = new JuegoMatematicas();
		boolean d = true;
		juego.jugar();
		Container cp = getContentPane();
		cp.setLayout(new GridLayout(5, 8));
		todo = new JLabel(juego.imprimir(), todo.CENTER);
		todo.setFont(new Font("Arial", Font.PLAIN, 40));
		tiemporestante = new JLabel("Tiempo restante: " + contador());
		tiemporestante.setFont(new Font("Arial", Font.PLAIN, 35));
		add(tiemporestante);
		tfCount = new JTextField("puntaje actual: ", 10);
		tfCount.setFont(new Font("Arial", Font.PLAIN, 35));
		tfCount.setEditable(false);
		add(tfCount);
		suma.setFont(new Font("Arial", Font.PLAIN, 35));
		resta.setFont(new Font("Arial", Font.PLAIN, 35));
		division.setFont(new Font("Arial", Font.PLAIN, 35));
		multiplicacion.setFont(new Font("Arial", Font.PLAIN, 35));
		add(todo);
		GridLayout c = new GridLayout(2, 2);
		Panel f = new Panel(c);
		f.setLayout(c);
		f.add(suma);
		/*
		 * cp.add(suma); cp.add(resta); cp.add(division);
		 * cp.add(multiplicacion);
		 */
		f.add(resta);
		f.add(multiplicacion);
		f.add(division);
		add(f);
		add(volver);
		new javax.swing.Timer(1, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (contador() > 0) {
					tiemporestante.setText("Tiempo Restante: " + contador());
				}
				if (contador() <= 0 && validador) {
					validador = false;
					tiemporestante.setText("fin del juego");
					setVisible(false);
					parar();
					SelectorJuego selector = new SelectorJuego(cliente,
							"Fin de juego, puntaje: " + count,count,0);
					// actualizar info del jugador
					// pje actual, pje promedio, pje maximo
				}
			}
		}).start();// metodo que no se puede sacar de este lugar ya que es un
					// evento por tiempo
	}

	@Override
	public void actionPerformed(ActionEvent evt) {

	}

	public void actionPerfomed(TextEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0); // terminate the program
	}

	@Override
	public void windowOpened(WindowEvent e) {
		JOptionPane
				.showMessageDialog(
						this,
						"Instrucciones : para jugar cliquea las operaciones, o bien presiona: \n (Q) para suma \n (W) para resta \n (E) para multiplicación \n (R) para division");
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	private class BtnListener implements ActionListener, KeyListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String btnLabel = e.getActionCommand();
			veces++;
			if (veces == 1) {
				timer.Contar();
			}
			if (btnLabel.equals(juego.devolverSigno())) {
				count += 4;
			} else
				count--;
			tfCount.setText("Puuntaje actual: " + count
					+ "          Intentos: " + veces);
			juego.generar();
			todo.setText(juego.imprimir());
			if (btnLabel == "Volver")
			// aca esta el loop que no podemos sacar
			{
				setVisible(false);
				validador = false;
				SelectorJuego selector = new SelectorJuego(cliente);
			}
			setFocusable(true);
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			char ch = e.getKeyChar();
			veces++;
			String s = "" + ch;
			if (s.equals("q") || s.equals("w") || s.equals("e")
					|| s.equals("r")) {
				s = juego.CambiarOperacion(s);
				if (veces == 1) {
					timer.Contar();
				}
				if (s.equals(juego.devolverSigno())) {
					count += 4;
				} else
					count--;
				tfCount.setText("Puuntaje actual: " + count
						+ "          Intentos: " + veces);
				juego.generar();
				todo.setText(juego.imprimir());
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	}

	public int gettiemporestante() {
		return count;
	}

	public int contador() {
		segundos = 30 - timer.getSegundos();
		return segundos;
	}

	void parar() {
		segundos = 10;
	}

}
