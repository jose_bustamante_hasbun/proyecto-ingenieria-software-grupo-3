package interfaz;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import Conexion.Cliente;
import Conexion.ClienteTicTac;
import Interaccion.Controlador;
import Logica.Tiempo;

public class VentanaTicTac extends Controlador implements ActionListener {

	String[] args;
	Cliente c;

	public VentanaTicTac(Cliente c) {
		super(c);
		this.c = c;
		args = new String[0];
		setVisible(false);
		try {
			conectar();
		} catch (Exception e) {
			System.exit(0);
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void empezar() {

	}

	private void conectar() throws Exception {
		MyRunnable myRunnable = new MyRunnable();
		Thread t = new Thread(myRunnable);
		t.start();

	}

	public class MyRunnable implements Runnable {

		public void run() {
			try {
				while (true) {
					String serverAddress = (args.length == 0) ? "localhost"
							: args[1];
					ClienteTicTac client = new ClienteTicTac(serverAddress,
							cliente);
					client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					client.frame.setSize(300, 200);
					client.frame.setVisible(true);
					client.play();
					
					int pje = 0;
					if (!client.wantsToPlayAgain()) {
						switch (client.win) {
						case 0:
							break;
						case 1:
							pje = 10;
						case 2:
							pje = 5;
						}
						cliente.puntaje.setPuntajeGato(pje);
						
						backToMenu(pje);
						break;
					} else {
						switch (client.win) {
						case 0:
							break;
						case 1:
							pje = 5;
						case 2:
							pje = 10;
						}
						cliente.puntaje.setPuntajeGato(pje);
					}
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	// /

	public void backToMenu(int pje) {
		MyRunnable2 myRunnable = new MyRunnable2(pje);
		Thread t = new Thread(myRunnable);
		t.start();

	}

	public class MyRunnable2 implements Runnable {

		int pje;

		public MyRunnable2(int pje) {
			this.pje = pje;
		}

		public void run() {
			/*
			 * ACA NO SE COMO VOLVER AL SELECTOR
			 */
			/*
			 * SelectorJuego vs = new SelectorJuego(cliente); vs.empezar();
			 * vs.setVisible(true);
			 */
			
			SelectorJuego selector = new SelectorJuego(cliente,
					"Fin de juego, puntaje: " + pje, pje, 0);

		}
	}
}
