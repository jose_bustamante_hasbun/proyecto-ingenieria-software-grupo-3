package interfaz;

import java.awt.Container;

import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;



public class VentanaPrincipal extends JFrame implements ActionListener, WindowListener{

	private JButton singlePlayer;
	private JButton multiPlayer;
	private JLabel bienvenida;

	public VentanaPrincipal(){

		Container cp = this.getContentPane();
		cp.setLayout(new GridLayout(2,1));
		Panel p1 = new Panel();
		Panel p2 = new Panel();
		singlePlayer = new JButton(new AbstractAction("Single Player"){
			@Override
			public void actionPerformed( ActionEvent e ) {
			VentanaInicio v = new VentanaInicio(true);
			setVisible(false);
			}
		});

		multiPlayer = new JButton(new AbstractAction("Multi Player"){
			@Override
			public void actionPerformed( ActionEvent e ) {
				
				VentanaSC vsc = new VentanaSC();
				setVisible(false);
				
			}
		});
		
		cp.add(p1);
		cp.add(p2);
		bienvenida = new JLabel("Bienvenidos a Bobugama");
		bienvenida.setFont(new Font("Arial",Font.BOLD,40));
		p1.add(bienvenida);
		p2.add(singlePlayer);
		p2.add(multiPlayer);

		setTitle("Bobugama");
		setSize(500, 300);
		setVisible(true);

	}


	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}



}
