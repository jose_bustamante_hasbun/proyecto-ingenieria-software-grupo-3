README 4.0
Bobugama Version 4.0
Cuarta realease de la aplicacion did�ctica en desarrollo. En esta versi�n se hizo un update del juego de matem�ticas, del juego de memoria y se agreg� el juego de Scramble. Se implementaron los requisitos funcionales siguientes: F1, F5, F9, F12, NF1, NF2, NF3, NF4, Nf5, NF6, NF7, NF8, NF11, NF13. Y los casos de uso 1, 3, 5, 6, 7, 8, 9.


#####################
PREVIOUS VERSIONS

Andres:

Al momento de iniciar la aplicaci�n, se abrir� una pantalla de bienvenida. Luego, se debe hacer click en el bot�n single player.
Una nueva ventana se abre, donde te pide usuario y contrase�a. Aqu� debes poner cualquier cosa para que no te pida reingresar los datos. Hacemos click en bot�n GO. Posterior a eso, se abrir� una ventana con los 4 juegos propuestos. Los que, hasta el momento, est�n implementados son Juego Matematicas y Juego Memorice.
Si quieres, puedes apretar en Juego Matem�ticas y te van a salir una serie de �ecuaciones� en las que tienes que poner la operaci�n en base a los n�meros que salgan. Si la respuesta fue correcta, se suma un punto al puntaje.
Ahora debes cerrar el programa, y hacer lo mismo solo que seleccionas el juego �Juego Memorice�.

Espero no existan problemas. Saludos.

PD: El archivo git.gitignore que se subi� con la entrega es para eliminar los archivos que no se deben subir.
